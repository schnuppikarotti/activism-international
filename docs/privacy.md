# Privacy in Activism.International

We see privacy as a universal right. This is why we try to minimize the data we collect. Anyway, for some of our services, collection of data is necessary.

For our different kinds of services, we have detailed privacy policies:

- [static services](privacy/static.md) (this dashboard)
- [temporary services](privacy/temporary.md) (everything you do not log in at, e.g. Pads, Jitsi calls or BigBlueButton meetings)
- [collaborative services](privacy/cloud.md) (everything you log in at, e.g. Activism Cloud or Matrix / Element)