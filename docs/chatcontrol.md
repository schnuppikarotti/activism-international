# \#ChatControl

## Who owns your chat?

**With *[#ChatControl](https://chatcontrol.eu)*, EU finally wants to *access your private chats*. Authorities shall screen for keywords and filter *unwanted or suspicious contents*.**

Let's fight it together!


What you can do is using **Matrix**, a federated and encrypted chat network.

[Learn more :material-information-outline:](/#our-tools){ .md-button }

[Start using Element now :material-download-outline:](https://matrix.activism.international/){ .md-button }

---

## Campaign material

This campaign material is free to use as in CC-0. We provide everything as PDF and PNG in both English and German. In case, you'd like to get material in your language, feel free to contact us.

You can find all our material at [https://cloud.activism.international/s/N57LxMTaN5c74HS](https://cloud.activism.international/s/N57LxMTaN5c74HS).

### English

![Poster English](https://cloud.activism.international/s/N57LxMTaN5c74HS/download?path=%2FFlyer%2FPNG&files=chatcontrol-general-en.png)

*Download English campaign poster as [Image](https://cloud.activism.international/s/N57LxMTaN5c74HS/download?path=%2FFlyer%2FPNG&files=chatcontrol-general-en.png) or [PDF](https://cloud.activism.international/s/N57LxMTaN5c74HS/download?path=%2FFlyer%2FPDF&files=chatcontrol-general-en.pdf).*

![Matrix English](https://cloud.activism.international/s/N57LxMTaN5c74HS/download?path=%2FFlyer%2FPNG&files=advantages-matrix-en.png)

*Download English Matrix flyer poster as [Image](https://cloud.activism.international/s/N57LxMTaN5c74HS/download?path=%2FFlyer%2FPNG&files=advantages-matrix-en.png) or [PDF](https://cloud.activism.international/s/N57LxMTaN5c74HS/download?path=%2FFlyer%2FPDF&files=advantages-matrix-en.pdf).*

### German

![Poster German](https://cloud.activism.international/s/N57LxMTaN5c74HS/download?path=%2FFlyer%2FPNG&files=chatcontrol-general-de.png)

*Download German campaign poster as [Image](https://cloud.activism.international/s/N57LxMTaN5c74HS/download?path=%2FFlyer%2FPNG&files=chatcontrol-general-de.png) or [PDF](https://cloud.activism.international/s/N57LxMTaN5c74HS/download?path=%2FFlyer%2FPDF&files=chatcontrol-general-de.pdf).*

![Matrix German](https://cloud.activism.international/s/N57LxMTaN5c74HS/download?path=%2FFlyer%2FPNG&files=advantages-matrix-de.png)

*Download English Matrix flyer as [Image](https://cloud.activism.international/s/N57LxMTaN5c74HS/download?path=%2FFlyer%2FPNG&files=advantages-matrix-de.png) or [PDF](https://cloud.activism.international/s/N57LxMTaN5c74HS/download?path=%2FFlyer%2FPDF&files=advantages-matrix-de.pdf).*