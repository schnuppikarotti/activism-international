![Logo featuring a fist inside clouds](assets/logo.svg){: loading=lazy align=left width=128 }

# Climate Justice. Let's shut shit down.

## The Activism Cloud

!!! question "**Looking for a unified way to organize the revolution?**"


    Activism.International provides online tools for secure communication, conferences and more to eco activists for free. In the [Activism Cloud](https://cloud.activism.international/), you can write encrypted messages ﹣even to people who do not use Activism Cloud﹣ , store files, create calendars and plan video conferences etc.

    Everyone can register, create a local group and collaborate in real-time. Using modern online office, you can take advanced notes, edit office documents, plan presentations, or share media files.
        
## Getting started

The [Activism Cloud](https://cloud.activism.international/) provides convenient access to numerous services with one single login, your Activism-ID.
Once registered, you can access all our services and enjoy a friendly environment for revolutionary organization.
You can also use many of our tools without creating an account.

[:material-account-check: Create Activism-ID](https://cloud.activism.international/apps/registration/){ .md-button .md-button--primary }
[:material-lock-open-variant: Use tools without Login](#our-tools){ .md-button }

**We do *not* ask you for a mail address, phone number or any other personal data during registration.**

If you have any questions or miss some features, feel free to [contact us](mailto:info@activism.international) or consult the [beginner's guide](https://cloud.activism.international/apps/onlyoffice/s/9bzbQgwBjy9e2Za).

## Our Tools



!!! danger ""
    
    **Activism Cloud** ﹣ Encrypted files, shared calendars and collaborative writing, integrates all our services at one unified place 

    [Open Activism Cloud :material-cloud:](https://cloud.activism.international/){ .md-button .md-button--primary }
    [Learn more :material-information-outline:](https://nextcloud.com/){ .md-button }

    :material-account-check: Activism-ID required [>> Register here](https://cloud.activism.international/apps/registration/)
    
    :material-domino-mask: Privacy: [Cloud policy](privacy/cloud.md) for your Activism-ID

!!! check ""
    
    **Matrix (Element)** ﹣ Encrypted messaging and internet telephony

    [Open in Activism Cloud :material-cloud:](https://cloud.activism.international/apps/riotchat){ .md-button .md-button--primary }
    [Open :octicons-link-external-16:](https://matrix.activism.international/){ .md-button }
    [Learn more :material-information-outline:](https://matrix.org/){ .md-button }

    :material-account-check: Activism-ID required [>> Register here](https://cloud.activism.international/apps/registration/)
    
    :material-domino-mask: [Cloud policy](privacy/cloud.md) for your Activism-ID

!!! note ""

    **BigBlueButton** ﹣ Video conferences with many participants, landline dial-in, slides and many extras
    
    [Open in Activism Cloud :material-cloud:](https://cloud.activism.international/apps/bbb){ .md-button .md-button--primary }
    [Open :octicons-link-external-16:](https://bbb.activism.international/){ .md-button }
    [Learn more :material-information-outline:](https://bigbluebutton.org/){ .md-button }    
    
    :material-lock-open-variant: No login needed. You can use BigBlueButton with your Activism-ID, if you want.
    
    :material-domino-mask: Privacy: [Temporary policy](privacy/temporary.md) (without login) or [Cloud policy](privacy/cloud.md) (with your Activism-ID)

!!! warning ""

    **Jitsi** ﹣ Simple and quick video chat
    
    [Open :octicons-link-external-16:](https://meet.activism.international/){ .md-button .md-button--primary }
    [ Learn more :material-information-outline:](https://meet.jit.si/){ .md-button }    
    
    :material-lock-open-variant: No login needed
    
    :material-domino-mask: Privacy: [Temporary policy](privacy/temporary.md)

!!! check ""

    **WireGuard** ﹣ Secure and performant VPN for private surfing in actions and at home
    
    [Open :octicons-link-external-16:](https://vpn.activism.international/){ .md-button .md-button--primary }
    [ Learn more :material-information-outline:](https://www.wireguard.com/){ .md-button }

    :material-account-check: Activism-ID required [>> Register here](https://cloud.activism.international/apps/registration/)
    
    :material-domino-mask: [Cloud policy](privacy/cloud.md) for your Activism-ID
    
!!! quote ""

    **Pad** ﹣ Shared notes
    
    [Open :octicons-link-external-16:](https://pad.activism.international/){ .md-button .md-button--primary }
    [Learn more :material-information-outline:](https://etherpad.org/){ .md-button }    
    
    
    ??? info "Use in Activism Cloud :material-cloud:"
        You can create a new pad [inside Activism Cloud](https://cloud.activism.international/apps/files) by clicking in the plus sign :octicons-plus-circle-16: on the top and choosing "Pad".
        
    :material-lock-open-variant: No login needed. However, an Activism-ID is required when using pads inside Activism Cloud [>> Register here](https://cloud.activism.international/apps/registration/)
    
    :material-domino-mask: Privacy: [Temporary policy](privacy/temporary.md) (without login) or [Cloud policy](privacy/cloud.md) (with your Activism-ID)

!!! danger ""
    
    **Livestream** ﹣ Live streaming server powered by Owncast 

    [Open Livestream :octicons-link-external-16:](https://live.activism.international/){ .md-button .md-button--primary }
    [Learn more :material-information-outline:](https://owncast.online/){ .md-button }

    :material-lock-open-variant: No login needed to watch. However, you need to contact us to stream yourself [info@activism.international](mailto:info@activism.international)
    
    :material-domino-mask: Privacy: [Temporary policy](privacy/temporary.md) (when not streaming)

!!! note ""
    
    **Invidious** ﹣ A tool to browse and watch YouTube videos without tracking or advertisements

    [Open Invidious :octicons-link-external-16:](https://invidious.activism.international/){ .md-button .md-button--primary }
    [Learn more :material-information-outline:](https://invidious.io/){ .md-button }

    :material-lock-open-variant: No login needed to watch.
    
    :material-domino-mask: Privacy: [Temporary policy](privacy/temporary.md)

## Mobile internet

We moreover provide mobile internet and WiFi on climate camps, direct actions etc.

Learn more [here](mobile-internet.md).


## How much may I use Activism Cloud ?

Any eco activist group complying with our [usage policy](usage-policy.md) may use Activism Cloud, even for private stuff. Per user, we initially provide 10 GB of online storage. If this is insufficient, you may contact us for more storage — we are solidary.

If you intensively use Activism Cloud, please consider supporting the solidary tech group by contributing with your local tech people (no need for deep knowledge or experience) or by [donating for our servers](about.md).

## Something is missing ?

Contact us [info@activism.international](mailto:info@activism.international).

## Security

Our servers are located in Hamburg / Germany and run on a Proxmox data center. All software we use is open source.

[Learn more on our security mechanisms](security.md)

## Who we are

Activism.International is provided by the solidary tech group, an informal association of activists with technical knowledge who want to share their abilities to everyone.

[Learn more](about.md)
